import TYPE from './actionTypes';
import { AppAction } from 'src/rootReducer';
import produce from 'immer';
export interface TodoItemInterface {
  id: string,
  label: string,
  deadline: string,
  isDone?: boolean,
};

interface ListTodoStore {
  listTodo: Readonly<Array<TodoItemInterface>>,
}

const initialState : ListTodoStore = {
  listTodo: [
    {
      id: "2021-03-21T18:28:00+07:00",
      isDone: false,
      label: "Second Item",
      deadline: "2021-03-22"
    },
    {
      id: "2021-03-21T18:27:50+07:00",
      isDone: false,
      label: "First Item",
      deadline: "2021-03-27"
    }
],
};

function findItemIndex(arrItem: Readonly<TodoItemInterface[]>, itemNewShape: TodoItemInterface): number {
  const targetItem = arrItem.find((item: TodoItemInterface) => item.id === itemNewShape.id);
  return arrItem.indexOf(targetItem as TodoItemInterface);
};

function updateItem(arrItem: Readonly<TodoItemInterface[]>, itemNewShape: TodoItemInterface) {
  const targetIndex = findItemIndex(arrItem, itemNewShape);
  return produce(arrItem, draft => {
    draft[targetIndex] = itemNewShape;
  });
};

function deleteItem(arrItem: Readonly<TodoItemInterface[]>, item: TodoItemInterface) {
  const targetIndex = findItemIndex(arrItem, item);
  return produce(arrItem, draft => {
    draft.splice(targetIndex, 1);
  });
};

export default function reducerTodo (
    state = initialState,
    {type, data}: AppAction
  ) {
  switch (type) {
    case TYPE.ADD_ITEM:
      return {
        ...state,
        listTodo: [ data, ...state.listTodo ]
      };
    case TYPE.UPDATE_ITEM:
      return {
        ...state,
        listTodo: updateItem(state.listTodo ,data)
      };
    case TYPE.REMOVE_ITEM:
      return {
        ...state,
        listTodo: deleteItem(state.listTodo ,data)
      }
    default:
      return state;
  }
}