import moment from 'moment';
import TYPE from './actionTypes';
import useStyles from './styles';
import MainModal from 'src/components/Modals';
import { TodoItemInterface } from './reducer';
import TodoItem from 'src/components/TodoItem';
import { dateFormat } from 'src/common/constants';
import useOpenModal from 'src/hooks/useOpenModal';
import { TextfieldLabel } from 'src/components/TextFieldLabel';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import React, { ChangeEventHandler, FormEvent, MouseEvent } from 'react';
import { Button, Container, Grid, MenuItem, Paper, Select, TextField } from '@material-ui/core';

enum FilterStatus {
  all = 'All',
  done = 'Done',
  undone = 'Undone'
}

export const meanTime = () => moment().format(dateFormat);
export const computeDeadline = (givenTime?: string, defaultTimeRange = 3) => {

  if (givenTime) {
    return moment(givenTime)
    .add(defaultTimeRange, 'days')
    .format(dateFormat);
  }
  return moment(meanTime())
  .add(defaultTimeRange, 'days')
  .format(dateFormat);
}

function Todo () {
  
  const [nameField, setNameField] = React.useState<string>('');
  const [dateField, setDateField] = React.useState<string>(computeDeadline());
  const [itemOnUpdate, setItemOnUpdate] = React.useState<TodoItemInterface>({} as TodoItemInterface);
  const [filterStatus, setFilterStatus] = React.useState<FilterStatus>(FilterStatus.all);
  
  const classes     = useStyles();
  const dispatch    = useDispatch();
  const isModalOpen = useOpenModal(itemOnUpdate);
  const listTodo    = useSelector((store: RootStateOrAny) => store.todo?.listTodo);

  const handleChangeDate: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement> = (e) => {
    const { value } = e.target;
    setDateField(value);
  };

  const handleFilterStatus = (e: any) => {
    const { value } = e.target;
    for (let key in FilterStatus) {
      const comparedValue = FilterStatus[key as keyof typeof FilterStatus]
      if (value === comparedValue) {
        setFilterStatus(FilterStatus[key as keyof typeof FilterStatus]);
        break;
      }
    }
  };

  console.log(filterStatus);

  const addNewItem = (e: FormEvent | MouseEvent<HTMLElement>) => {
    e.preventDefault();
    const id = moment().format();
    const action = {
      type: TYPE.ADD_ITEM,
      data: {
        id,
        isDone: false,
        label: nameField,
        deadline: dateField,
      },

    };
    if (nameField) {
      dispatch(action);
      setNameField('');
      setDateField(computeDeadline());
    }
  };

  const updateItem = (newShape: TodoItemInterface) => {
    const action = {
      type: TYPE.UPDATE_ITEM,
      data: newShape
    };
    dispatch(action);
  };

  const handleDelete = (item: TodoItemInterface) => {
    const action = {
      type: TYPE.REMOVE_ITEM,
      data: item
    };
    dispatch(action);
    setItemOnUpdate({} as TodoItemInterface)
  };

  return (
    <Container className={classes.root}>
      <h1>Future is waiting to be completed</h1>
      <Grid container justify="center" className="area_common area_input">
        <h2>Please input new item here</h2>
        <Grid container justify="center">
          <form onSubmit={addNewItem}>
            <Grid
              container
              justify="center"
              alignItems="center"
              spacing={5}
            >
              <Grid item xs={12} sm={4}>
                <TextField
                  name="item"
                  type="input"
                  autoFocus={true}
                  value={nameField}
                  placeholder="Eg: Make the test"
                  InputLabelProps={{ shrink: true }}
                  label={<TextfieldLabel text="Item's name"/>}
                  onChange={e => setNameField(e.target.value)}
                />
              </Grid>
              <Grid item  xs={12} sm={4}>
                <TextField
                    type="date"
                    onChange={handleChangeDate}
                    InputLabelProps={{ shrink: true }}
                    inputProps={{
                      min: meanTime()
                    }}
                    value={dateField}
                    label={<TextfieldLabel text="Deadline"/>}
                  />
              </Grid>
              <Grid container xs={12} justify="center">
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={!nameField}
                >
                  Add new item
                </Button>
              </Grid>
            </Grid>
          </form>
          
        </Grid>
      </Grid>
      <Grid container justify="center" className="area_common area_list">
        <h2>List of items</h2>
        <div>
        <Grid
          container
          spacing={5}
          justify="center"
          alignItems="center"
        >
          <Grid item><TextfieldLabel text="Select Status:" /></Grid>
          <Grid item>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={filterStatus}
              onChange={handleFilterStatus}
            >
              <MenuItem value={FilterStatus.all}>All</MenuItem>
              <MenuItem value={FilterStatus.done}>Done</MenuItem>
              <MenuItem value={FilterStatus.undone}>Undone</MenuItem>
            </Select>
          </Grid>
        </Grid>
      </div>
      </Grid>
      <Grid
        container
        justify="center"
        alignItems="center"
      >
        {(filterStatus === FilterStatus.all) && 
          listTodo.map((ele: TodoItemInterface) => (
            <Grid
              container
              key={ele.id}
              justify="center"
              alignItems="center"
            >
              <Paper elevation={8} style={{marginTop: '30px'}} >
                <TodoItem
                  openModalEdit={() => setItemOnUpdate(ele)}
                  handleStatus={updateItem} 
                  item={ele}
                />
              </Paper>
            </Grid>
          ))
        }
        {(filterStatus === FilterStatus.done) && 
          listTodo.filter((item: TodoItemInterface) => (item.isDone))
          .map((ele: TodoItemInterface) => {
            return (
              <Grid
                container
                key={ele.id}
                justify="center"
                alignItems="center"
              >
                <Paper elevation={8} style={{marginTop: '30px'}} >
                  <TodoItem
                    openModalEdit={() => setItemOnUpdate(ele)}
                    handleStatus={updateItem} 
                    item={ele}
                  />
                </Paper>
              </Grid>
            )
          })
        }
        {(filterStatus === FilterStatus.undone) && 
          listTodo.filter((item: TodoItemInterface) => (!item.isDone))
          .map((ele: TodoItemInterface) => {
            return (
              <Grid
                container
                key={ele.id}
                justify="center"
                alignItems="center"
              >
                <Paper elevation={8} style={{marginTop: '30px'}} >
                  <TodoItem
                    openModalEdit={() => setItemOnUpdate(ele)}
                    handleStatus={updateItem} 
                    item={ele}
                  />
                </Paper>
              </Grid>
            )
          })
        }
      </Grid>
      {
        isModalOpen && (
          <MainModal
            item={itemOnUpdate}
            handleDelete={handleDelete}
            handleUpdate={updateItem}
            handleClose={() => setItemOnUpdate({} as TodoItemInterface)}
          />
        )
      }
    </Container>
  );
};

export default React.memo(Todo);