import { makeStyles } from "@material-ui/core";

const styles = {
  root: {
    padding: '50px 0',
    '& form': {
      height: '150px'
    },
    '& .area_common ': {
      '& > h2, & > div': {
        width: '500px',
        textAlign: 'left',
      },
    },
    '& label + .MuiInput-formControl': {
      marginTop: '30px'
    },
    '& .area_list': {
      marginTop: '10px',

    },
    '& .area_input': {
      '&::after': {
        content: '',
        display: 'block',
        position: 'relative',
        width: '100%',
        height: '2px',
        background: 'blue'
      }
    },
  }
};

const useStyles = makeStyles(styles);

export default useStyles;
