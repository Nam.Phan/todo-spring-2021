const styles = {
  root: {
    '& .display_none': {
      display: 'none'
    },
    '& h1, h2, h3': {
      color: '#545E75'
    },
  }
};

export default styles;