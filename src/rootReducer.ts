import { CombinedState, combineReducers, Action } from 'redux';
import todoReducer, { TodoItemInterface } from 'src/screens/Todo/reducer';

export interface AppAction extends Action<string> {data: TodoItemInterface}

const initialState = {} as CombinedState<{ todo: unknown; }>

// export function createReducer () {
  
//   const rootReducer = (state = initialState, action: AppAction ) => combineReducers(allReducer)(state as any, action);
//   return rootReducer;
// };

const allReducers = {
  todo: todoReducer
}

const rootReducer = (state = initialState, action: AppAction ) => combineReducers(allReducers)(state as any, action);

export default rootReducer;