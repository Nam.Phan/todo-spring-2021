import React from "react";
import { TodoItemInterface } from "src/screens/Todo/reducer";


export default function useOpenModal(item: TodoItemInterface) {
  const [isModalOpen, setModalOpen] = React.useState(false);
  React.useEffect(() => {
    if (item.id === undefined) {
      setModalOpen(false);
    } else {
      setModalOpen(true);
    }
  }, [item]);

  return isModalOpen;
}