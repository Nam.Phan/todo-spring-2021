export const TextfieldLabel = ({text}: {text: string}) => (
  <h3>
    {text}
  </h3>
);