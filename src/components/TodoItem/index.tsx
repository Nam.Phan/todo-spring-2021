import moment from 'moment';
import useStyles from './styles';
import React, { MouseEventHandler } from 'react';
import { dateFormat } from 'src/common/constants';
import CreateIcon from '@material-ui/icons/Create';
import CheckBoxIcon from '@material-ui/icons/CheckBox';
import { Button, Grid, TextField } from '@material-ui/core';
import { TodoItemInterface } from 'src/screens/Todo/reducer';
import { computeDeadline as computeWarning } from 'src/screens/Todo';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';

interface PropTypes {
  item: TodoItemInterface,
  handleStatus: Function,
  openModalEdit?: MouseEventHandler<HTMLButtonElement>
};



function TodoItem(props: PropTypes) {
  const {
    item,
    handleStatus,
    openModalEdit
  } = props;

  const {
    isDone,
    deadline,
    label: itemLabel
  } = item;

  const classes = useStyles();
  const today = moment().format(dateFormat);
  const shouldWarning = moment(computeWarning(today, 1)) >= moment(deadline);

  return (
    <Grid container className={classes.root}>
      <div className="item_info">
        <TextField
          type="text"
          value={itemLabel || ""}
          disabled
        />
        <TextField
          className={`date_picker ${shouldWarning ? 'warning' : ''}`}
          id="date"
          label="Deadline"
          type="date"
          disabled
          value={deadline}
          InputLabelProps={{
            shrink: true,
            className: 'display_none'
          }}
        />
      </div>
      <div className="wrap_btn">
        <Button type="button" onClick={openModalEdit}>
          <CreateIcon />
        </Button>
        <Button
          type="button"
          onClick={() => handleStatus({
            ...item,
            isDone: !item.isDone
          })}
        >
          { !isDone ? <CheckBoxOutlineBlankIcon /> : <CheckBoxIcon />}
        </Button>
      </div>
    </Grid>
  )
};

export default TodoItem;