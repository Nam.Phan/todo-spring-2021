import { makeStyles } from "@material-ui/core";

const styles = {
  root: {
    display: 'flex',
    padding: '15px 10px',
    '& .item_info': {
      display: 'inline-flex',
      flexDirection: 'column',
      width: '50%'
    },
    '& .wrap_btn': {
      display: 'flex',
      justifyContent: 'flex-end',
      width: '50%'
    },
    '& .date_picker': {
      '&.warning .MuiInputBase-input': {
        color: '#f44336!important',
        fontSize: '18px',
        fontWeight: 900
      },
    },
    '& .date_picker .MuiInput-formControl': {
      marginTop: 0
    },
    '& .MuiInput-underline.Mui-disabled:before': {
      'border-bottom-style': 'hidden'
    },
    '& .MuiInputBase-input.Mui-disabled': {
      '&[type="text"]': {
        color: '#1919e4',
        fontWeight: 900,
        fontSize: '20px'
      },
      '&[type="date"]': {
        color: '#303f9f'
      }
    }

  }
};

const useStyles = makeStyles(styles);

export default useStyles;