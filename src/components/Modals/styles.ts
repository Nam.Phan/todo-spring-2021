import { makeStyles } from "@material-ui/core";

const styles = {
  root: {
    '& .modal_content': {
      background: '#fff',
      borderRadius: '10px',
      padding: '20px 10px',
      width: '700px',
      maxWidth: '95%',
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)'
    },
    '& label + .MuiInput-formControl': {
      marginTop: '30px'
    },
    '& button + button': {
      marginLeft: '20px'
    },
    '& .modal_body': {
      padding: '20px 0' 
    },

    '& .MuiTextField-root:nth-child(2)': {
      marginLeft: '30px'
    },

    '& .modal_footer': {
      padding: '20px 30px 10px' 
    }
  }
};

const useStyles = makeStyles(styles);

export default useStyles;