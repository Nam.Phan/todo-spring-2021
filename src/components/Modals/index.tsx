import useStyles from './styles';
import { meanTime } from 'src/screens/Todo';
import SaveIcon from '@material-ui/icons/Save';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import { TextfieldLabel } from '../TextFieldLabel';
import { TodoItemInterface } from 'src/screens/Todo/reducer';
import CancelIcon from '@material-ui/icons/CancelPresentation';
import React, { ChangeEventHandler, MouseEventHandler } from 'react';
import { Button, Container, Grid, Modal, TextField } from '@material-ui/core';


interface Proptypes {
  item: TodoItemInterface,
  handleUpdate: Function,
  handleClose: MouseEventHandler,
  handleDelete: (item: TodoItemInterface) => void
};

function MainModal(props: Proptypes) {
  const { item, handleClose, handleUpdate, handleDelete } = props;
  const { label: itemLabel, deadline } = item;

  const [nameField, setNameField] = React.useState<string>(itemLabel);
  const [dateField, setDateField] = React.useState<string>(deadline);

  const classes = useStyles();

  const handleModalUpdate: MouseEventHandler = e => {
    handleUpdate({
      ...item,
      label: nameField,
      deadline: dateField,
    });
    handleClose(e)
  }

  const handleEditName: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement> = e => {
    const { value } = e.target;
    setNameField(value);
  };

  const handleChangeDate: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>  = e => {
    const { value } = e.target;
    setDateField(value);
  }

  return (
    <Modal
      open={true}
      onClose={handleClose}
      aria-labelledby="modal-title"
      aria-describedby="modal-description"
      className={classes.root}
    >
      <Grid className="modal_content">
        <Grid container justify="flex-end" className="modal_header">
          <Button
            // variant="contained"
            size="large"
            startIcon={<CloseIcon/>}
            onClick={handleClose} 
          />
        </Grid>
        <Container className="modal_body">
          <Grid
            container
            justify="center"
            alignItems="center"
          >
            <TextField
              autoFocus={true}
              value={nameField}
              onChange={handleEditName}
              placeholder={itemLabel}
              InputLabelProps={{ shrink: true }}
              label={<TextfieldLabel text="Item"/>}
            />
            <TextField
              type="date"
              inputProps={{
                min: meanTime()
              }}
              autoFocus={true}
              value={dateField}
              onChange={handleChangeDate}
              InputLabelProps={{ shrink: true }}
              label={<TextfieldLabel text="Date"/>}
            />
          </Grid>
        </Container>
        <Container className="modal_footer">
          <Grid container justify="space-between">
            <Button
              variant="contained"
              color="secondary"
              size="large"
              startIcon={<DeleteIcon />}
              onClick={_ => handleDelete(item)}
            >
              Delete
            </Button>
            <Grid item>
              <Button
                variant="contained"
                size="large"
                onClick={handleClose}
                startIcon={<CancelIcon />}
              >
                Cancel
              </Button>
              <Button
                variant="contained"
                color="primary"
                size="large"
                // className={classes.button}
                onClick={handleModalUpdate}
                startIcon={<SaveIcon />}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </Container>
      </Grid>
    </Modal>
  );
}

export default MainModal;