import React from 'react';
import './App.css';
import Todo from './screens/Todo';
import { makeStyles } from '@material-ui/core';
import styles from './AppStyles';

const useStyles = makeStyles(styles);

function App() {
  const styleClass = useStyles();

  return (
    <div className={`App ${styleClass.root}`}>
      <Todo />
    </div>
  );
}

export default App;
